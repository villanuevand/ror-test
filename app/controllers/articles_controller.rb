class ArticlesController < ApplicationController
	#New
	def new 
		@article = Article.new
	end

	#Create
	def create
		@article = Article.new(article_params)

		if @article.save
			redirect_to @article
		else
			render 'new'
		end
		#render plain: params[:article].inspect
	end

	#Show
  	def show
  		@article = Article.find(params[:id])
  	end

  	#Index
  	def index
  		@articles = Article.all
  	end

  	#Edit
  	def edit
	  @article = Article.find(params[:id])
	end

	#Update
	def update
	  @article = Article.find(params[:id])
	 
	  if @article.update(article_params)
	    redirect_to @article
	  else
	    render 'edit'
	  end
	end

	#Destroy
	def destroy
	  @article = Article.find(params[:id])
	  @article.destroy
	 
	  redirect_to articles_path
	end

  	private
  		def article_params
    		params.require(:article).permit(:title, :text)
  		end
end
